//
//  AppDelegate.h
//  Section 4 general example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

