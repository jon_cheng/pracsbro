//
//  ViewController.m
//  Section 4 general example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.Hiddenview.hidden = YES;
    self.Segmentoutlet.selectedSegmentIndex = 0;
    
    PickerData = [[NSArray alloc] initWithObjects:@"Row 1",@"Row 2",@"Row 3",@"Row 4",@"Row 5",@"Row 6", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [PickerData count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [PickerData objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch ((int)row) {
        case 0:
            self.displaytext.text = @"0";
            break;
        case 1:
            self.displaytext.text = @"1";
            break;
        case 2:
            self.displaytext.text = @"2";
            break;
        case 3:
            self.displaytext.text = @"3";
            break;
            
        default:
            self.displaytext.text = @"default";
            break;
    }
}

- (IBAction)Segmentaction:(id)sender {
    switch (self.Segmentoutlet.selectedSegmentIndex) {
        case 0:
            self.displaytext.hidden = YES;
            self.DisplayScrollview.hidden = NO;
            break;
        case 1:
            self.displaytext.hidden = NO;
            self.DisplayScrollview.hidden = YES;
            break;
            
        default:
            self.displaytext.hidden = YES;
            self.DisplayScrollview.hidden = YES;
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [self.DisplayScrollview setScrollEnabled:YES];
    //[self.DisplayScrollview setContentSize:CGSizeMake(0, 500)];
}

- (void)viewDidLayoutSubviews {
    //[self.DisplayScrollview setContentSize:CGSizeMake(0, 500)];
}

- (IBAction)Showview:(id)sender {
    self.Hiddenview.hidden = NO;
}

- (IBAction)Hideview:(id)sender {
    self.Hiddenview.hidden = YES;
}
- (IBAction)DisplayAlertBtn:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"title" message:@"press okay" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        self.displaytext.text = @"button pressed";
    }];
    
    [alert addAction:ok];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
