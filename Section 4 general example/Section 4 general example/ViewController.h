//
//  ViewController.h
//  Section 4 general example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSArray *PickerData;
}

//segment control
@property (weak, nonatomic) IBOutlet UISegmentedControl *Segmentoutlet;
- (IBAction)Segmentaction:(id)sender;

//btn for controls
- (IBAction)Showview:(id)sender;
- (IBAction)Hideview:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *Hiddenview;

//to display textfield and scrollview
@property (weak, nonatomic) IBOutlet UITextView *displaytext;
@property (weak, nonatomic) IBOutlet UIScrollView *DisplayScrollview;

//for alert
@property (weak, nonatomic) IBOutlet UIButton *DisplayAlertOutlet;
- (IBAction)DisplayAlertBtn:(id)sender;

//pickerview demo
@property (weak, nonatomic) IBOutlet UIPickerView *PickerView;

@end

