//
//  ViewController.m
//  example1
//
//  Created by ParkEasy on 9/2/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)leftbtn:(id)sender {
    self.lbl1.textAlignment = NSTextAlignmentLeft;
}
- (IBAction)centerbtn:(id)sender {
    self.lbl1.textAlignment = NSTextAlignmentCenter;
}
- (IBAction)rightbtn:(id)sender {
    self.lbl1.textAlignment = NSTextAlignmentRight;
}
- (IBAction)button2:(id)sender {
    self.lbl1.textColor = [UIColor redColor];
}
- (IBAction)button3:(id)sender {
    [self.lbl1 setFont:[UIFont fontWithName:@"Verdana" size:25]];
}
- (IBAction)displaytext:(id)sender {
    self.lbl1.text = self.text1.text;
    [self resignFirstResponder];
}

@end
