//
//  ViewController.h
//  example1
//
//  Created by ParkEasy on 9/2/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UITextField *text1;

- (IBAction)leftbtn:(id)sender;
- (IBAction)centerbtn:(id)sender;
- (IBAction)rightbtn:(id)sender;
- (IBAction)button2:(id)sender;
- (IBAction)button3:(id)sender;

@end

