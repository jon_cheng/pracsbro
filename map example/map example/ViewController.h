//
//  ViewController.h
//  map example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    
}
@property (weak, nonatomic) IBOutlet MKMapView *Mapview;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

- (IBAction)ChangeMap:(id)sender;
- (IBAction)LocateMe:(id)sender;

- (IBAction)RouteBtn:(id)sender;


@end

