//
//  ViewController.m
//  reactionexample
//
//  Created by ParkEasy on 9/2/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    scoreInt = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Startbtn:(id)sender {
    
    if (scoreInt == 0) {
        
        self.startbtn.enabled = NO;
        
        CountInt = 3;
        self.lbl1.text = [NSString stringWithFormat:@"starting in.. %i", CountInt];
        self.Scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
    
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(CountTimer) userInfo:nil repeats:YES];
        
        [sender setTitle:@"stop" forState:UIControlStateNormal];
    } else {
        [timerScore invalidate];
    }
    
    if (CountInt == 0) {
        scoreInt = 0;
        
        self.lbl1.text = [NSString stringWithFormat:@"Stopped!"];
        [sender setTitle:@"restart" forState:UIControlStateNormal];
    }
}


-(void)CountTimer {
    
    CountInt -= 1;
    self.lbl1.text = [NSString stringWithFormat:@"starting in.. %i", CountInt];
    
    if (CountInt < 1 ) {
        [timer invalidate];
        self.startbtn.enabled = YES;
        self.lbl1.text = [NSString stringWithFormat:@"Started!"];
        
        scoreInt = 0;
        self.Scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
        
        timerScore = [NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(ScoreTimer) userInfo:nil repeats:YES];
        
    } else {
        [timerScore invalidate];
    }
}

-(void)ScoreTimer {
    scoreInt += 1;
    self.Scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
}

@end
