//
//  ViewController.m
//  shakeme example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    CountInt = 10;
    ScoreInt = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)StartBtn:(id)sender {
    if (CountInt == 10) {
        self.Timerlbl.text = [NSString stringWithFormat:@"ending in.. %i", CountInt];
        ScoreInt = 0;
        self.Scorelbl.text = [NSString stringWithFormat:@"Score: %i", ScoreInt];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(StartCounter) userInfo:nil repeats:YES];
        ModeInt = 1;
        self.Startoutlet.enabled = NO;
        [sender setTitle:@"SHAKE!" forState:UIControlStateNormal];
        
        [self performSelector:@selector(Delay) withObject:nil afterDelay:5.0];
    }
}

- (void)Delay {
    self.Nooblbl.text = @"YOU NOOB";
}

- (void)StartCounter {
    CountInt -= 1;
    self.Timerlbl.text = [NSString stringWithFormat:@"ending in.. %i", CountInt];
    
    if (CountInt < 1) {
        [timer invalidate];
        self.Nooblbl.text = @" ";
        CountInt = 10;
        ModeInt = 2;
        self.Startoutlet.enabled = YES;
        self.Timerlbl.text = [NSString stringWithFormat:@"Ready again?"];
        [self.Startoutlet setTitle:@"Start!" forState:UIControlStateNormal];
    }
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.subtype == UIEventSubtypeMotionShake && ModeInt == 1) {
        ScoreInt += 1;
        self.Scorelbl.text = [NSString stringWithFormat:@"Score: %i", ScoreInt];
    }
}
@end
