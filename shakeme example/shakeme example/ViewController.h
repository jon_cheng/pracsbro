//
//  ViewController.h
//  shakeme example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSTimer *timer;
    
    int CountInt;
    int ScoreInt;
    int ModeInt;
}

@property (weak, nonatomic) IBOutlet UILabel *Timerlbl;
@property (weak, nonatomic) IBOutlet UILabel *Scorelbl;

@property (weak, nonatomic) IBOutlet UIButton *Startoutlet;
- (IBAction)StartBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *Nooblbl;

@end

