//
//  ViewController.h
//  section 5 example pics and import
//
//  Created by ParkEasy on 9/7/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *ImageView;

- (IBAction)Display1:(id)sender;
- (IBAction)Display2:(id)sender;
- (IBAction)DisplayURL:(id)sender;

@end

