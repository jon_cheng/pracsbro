//
//  ViewController.m
//  section 5 example pics and import
//
//  Created by ParkEasy on 9/7/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Display1:(id)sender {
    self.ImageView.image = [UIImage imageNamed:@"dogs/dog1.png"];
}

- (IBAction)Display2:(id)sender {
    self.ImageView.image = [UIImage imageNamed:@"dogs/dog2.png"];
}

- (IBAction)DisplayURL:(id)sender {
    self.ImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.canggupetresort.com/images/diet-pill-dog.png"]]];
}
@end
