//
//  ViewController.m
//  segment example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentBtn:(id)sender {
    switch (self.segmentoutlet.selectedSegmentIndex) {
        case 0:
            self.displaylbl.text = @"segment first";
            break;
        case 1:
            self.displaylbl.text = @"segment second";
            break;
        case 2:
            self.displaylbl.text = @"segment third";
            break;
        case 3:
            self.displaylbl.text = @"segment fourth";
            break;
            
        default:
            break;
    }
}
- (IBAction)Slider1:(id)sender {
    [self.sliderdisplay setFont:[UIFont fontWithName:@"Verdana" size:self.Slideroutlet.value]];
}
@end
