//
//  ViewController.h
//  segment example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentoutlet;
- (IBAction)segmentBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *displaylbl;

@property (weak, nonatomic) IBOutlet UISlider *Slideroutlet;
- (IBAction)Slider1:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *sliderdisplay;

@end

