//
//  ViewController.h
//  tappingnumber example
//
//  Created by ParkEasy on 9/3/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSTimer *timer;
    int CountInt;
    int scoreInt;
    float speedlvl;
    int valuecompare;
    int RandomNumber;
}

@property (weak, nonatomic) IBOutlet UILabel *scorelbl;
@property (weak, nonatomic) IBOutlet UILabel *timerlbl;
@property (weak, nonatomic) IBOutlet UILabel *randomlbl;
- (IBAction)Startbtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Startoutlet;
@property (weak, nonatomic) IBOutlet UILabel *statuslbl;

- (IBAction)onebtn:(id)sender;
- (IBAction)twobtn:(id)sender;
- (IBAction)threbtn:(id)sender;
- (IBAction)forbtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *oneoutlet;
@property (weak, nonatomic) IBOutlet UIButton *twooutlet;
@property (weak, nonatomic) IBOutlet UIButton *threoutlet;
@property (weak, nonatomic) IBOutlet UIButton *foroutlet;

- (IBAction)lvlincre:(UIStepper *)sender;
@property (weak, nonatomic) IBOutlet UIStepper *lvloutlet;
@property (weak, nonatomic) IBOutlet UILabel *lvllbl;
@end

