//
//  ViewController.m
//  tappingnumber example
//
//  Created by ParkEasy on 9/3/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib
    //scoreInt = 0;
    CountInt = 100;
    speedlvl = 0.5;
    //valuecompare = 0;
    self.oneoutlet.enabled = NO;
    self.twooutlet.enabled = NO;
    self.threoutlet.enabled = NO;
    self.foroutlet.enabled = NO;
    self.Startoutlet.enabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Startbtn:(id)sender {
    NSLog(@"startbutton");
    if (CountInt == 100) {
        scoreInt = 0;
        valuecompare = 0;
        self.timerlbl.text = [NSString stringWithFormat:@"ending in.. %i", CountInt];
        //self.scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
        //RandomNumber = 1 + arc4random() % 4;
        //self.randomlbl.text = [[NSString alloc] initWithFormat:@"%d", RandomNumber];
        //valuecompare = RandomNumber;
        timer = [NSTimer scheduledTimerWithTimeInterval:speedlvl target:self selector:@selector(StartCounter) userInfo:nil repeats:YES];
        
        self.oneoutlet.enabled = YES;
        self.twooutlet.enabled = YES;
        self.threoutlet.enabled = YES;
        self.foroutlet.enabled = YES;
        self.Startoutlet.enabled = NO;
        [sender setTitle:@"GO!" forState:UIControlStateNormal];
    }
}

- (void)StartCounter {
    self.oneoutlet.enabled = YES;
    self.twooutlet.enabled = YES;
    self.threoutlet.enabled = YES;
    self.foroutlet.enabled = YES;
    
    RandomNumber = 1 + arc4random() % 4;
    self.randomlbl.text = [[NSString alloc] initWithFormat:@"%d", RandomNumber];
    
    CountInt -= 1;
    self.timerlbl.text = [NSString stringWithFormat:@"ending in.. %i", CountInt];
    
    if (CountInt < 1) {
        [timer invalidate];
        CountInt = 100;
        self.Startoutlet.enabled = YES;
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
        
        self.statuslbl.textColor = [UIColor blackColor];
        self.statuslbl.text = @"--";
        
        self.timerlbl.text = @"Ready again?";
        self.randomlbl.text = @"--";
        [self.Startoutlet setTitle:@"Play" forState:UIControlStateNormal];
    }
}

- (IBAction)onebtn:(id)sender {
    NSLog(@"one button");
    if (RandomNumber == 1) {
        scoreInt += 1;
        self.scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
        self.statuslbl.textColor = [UIColor greenColor];
        self.statuslbl.text = @"+1";
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
    } else {
        self.statuslbl.textColor = [UIColor redColor];
        self.statuslbl.text = @"wrong!";
        
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
    }
}
- (IBAction)twobtn:(id)sender {
    NSLog(@"two button");
    if (RandomNumber == 2) {
        //[self adding];
        scoreInt += 1;
        self.scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
        self.statuslbl.textColor = [UIColor greenColor];
        self.statuslbl.text = @"+1";
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
    } else {
        self.statuslbl.textColor = [UIColor redColor];
        self.statuslbl.text = @"wrong!";
        
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
    }
}
- (IBAction)threbtn:(id)sender {
    NSLog(@"three button");
    if (RandomNumber == 3) {
        //[self adding];
        scoreInt += 1;
        self.scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
        self.statuslbl.textColor = [UIColor greenColor];
        self.statuslbl.text = @"+1";
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
        
    } else {
        self.statuslbl.textColor = [UIColor redColor];
        self.statuslbl.text = @"wrong!";
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
        
    }
}
- (IBAction)forbtn:(id)sender {
    NSLog(@"four button");
    if (RandomNumber == 4) {
        //[self adding];
        scoreInt += 1;
        self.scorelbl.text = [NSString stringWithFormat:@"Score: %i", scoreInt];
        
        self.statuslbl.textColor = [UIColor greenColor];
        self.statuslbl.text = @"+1";
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
        
    } else {
        self.statuslbl.textColor = [UIColor redColor];
        self.statuslbl.text = @"wrong!";
        
        self.oneoutlet.enabled = NO;
        self.twooutlet.enabled = NO;
        self.threoutlet.enabled = NO;
        self.foroutlet.enabled = NO;
    }
}

- (IBAction)lvlincre:(UIStepper *)sender {
    NSString* formattedNumber = [NSString stringWithFormat:@"%.1f", sender.value];
    self.lvllbl.text = formattedNumber;
    speedlvl = sender.value;
}
@end
