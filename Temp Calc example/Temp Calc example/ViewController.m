//
//  ViewController.m
//  Temp Calc example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Slideraction:(id)sender {
    self.Farenheitlbl.text = [[NSString alloc] initWithFormat:@"Fahrenheit: %4.0f F", self.Slideroutlet.value];
    
    double Fahrenheit = self.Slideroutlet.value;
    double Celcius = (Fahrenheit - 32) / 1.8;
    
    NSString *result = [[NSString alloc] initWithFormat:@"Fahrenheit: %4.1f C", Celcius];
    
    self.Celciuslbl.text = result;
}
@end
