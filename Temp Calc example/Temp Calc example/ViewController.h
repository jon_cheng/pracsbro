//
//  ViewController.h
//  Temp Calc example
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *Celciuslbl;
@property (weak, nonatomic) IBOutlet UILabel *Farenheitlbl;

@property (weak, nonatomic) IBOutlet UISlider *Slideroutlet;
- (IBAction)Slideraction:(id)sender;

@end

