//
//  ViewController.m
//  extra view example
//
//  Created by ParkEasy on 9/7/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"
#import "XIBViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)SwitchXIBbtn:(id)sender {
    XIBViewController *XIBView = [[XIBViewController alloc] initWithNibName:nil bundle:nil];
    XIBView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:XIBView animated:YES completion:NULL];
}
@end
