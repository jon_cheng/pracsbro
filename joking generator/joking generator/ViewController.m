//
//  ViewController.m
//  joking generator
//
//  Created by ParkEasy on 9/3/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Jokebtn:(id)sender {
    NSString *fileContents = [[NSBundle mainBundle] pathForResource:@"JokeList" ofType:@"plist"];
    NSDictionary *wordDIC = [[NSDictionary alloc] initWithContentsOfFile:fileContents];
    
    NSMutableArray *items = [wordDIC valueForKey:@"Jokes"];
    int RandomJoke = arc4random() % [items  count];
    NSString *Word = [items objectAtIndex:RandomJoke];
    
    [self.jokelbl setText:[[NSString alloc] initWithFormat:@"%@", Word]];
}
@end
