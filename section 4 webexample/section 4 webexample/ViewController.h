//
//  ViewController.h
//  section 4 webexample
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSTimer *timer;
}

@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;
@property (weak, nonatomic) IBOutlet UIWebView *WebView1;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoaderOutlet;
@end

