//
//  ViewController.m
//  section 4 webexample
//
//  Created by ParkEasy on 9/4/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.SearchBar.text = @"http://google.com";
    [self.WebView1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    [self.WebView1 addSubview:self.LoaderOutlet];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0/2.0 target:self selector:@selector(loading) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loading {
    if (!self.WebView1.loading) {
        [self.LoaderOutlet stopAnimating];
    } else {
        [self.LoaderOutlet startAnimating];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.WebView1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@", self.SearchBar.text]]]];
    [self.WebView1 addSubview:self.LoaderOutlet];
    [self.SearchBar resignFirstResponder];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0/2.0 target:self selector:@selector(loading) userInfo:nil repeats:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.SearchBar.text = nil;
    [self.SearchBar resignFirstResponder];
}

@end
