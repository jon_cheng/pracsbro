//
//  ViewController.h
//  calc assignment
//
//  Created by ParkEasy on 9/3/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSString *stackValues;
    NSString *OperatorStack;
    int operatortype;
    double Result;
}

- (IBAction)Onebtn:(id)sender;
- (IBAction)Twobtn:(id)sender;
- (IBAction)Threebtn:(id)sender;
- (IBAction)Fourbtn:(id)sender;
- (IBAction)Fivebtn:(id)sender;
- (IBAction)Sixbtn:(id)sender;
- (IBAction)Sevenbtn:(id)sender;
- (IBAction)Eightbtn:(id)sender;
- (IBAction)Ninebtn:(id)sender;
- (IBAction)Zerobtn:(id)sender;
- (IBAction)Dotbtn:(id)sender;

- (IBAction)ClearAllbtn:(id)sender;
- (IBAction)ClearBtn:(id)sender;
- (IBAction)Plusbtn:(id)sender;
- (IBAction)Minusbtn:(id)sender;
- (IBAction)Timesbtn:(id)sender;
- (IBAction)Dividebtn:(id)sender;
- (IBAction)Calculatebtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *Displaylbl;
@property (weak, nonatomic) IBOutlet UILabel *CalculatedDisplay;


@end

