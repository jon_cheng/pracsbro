//
//  ViewController.m
//  calc assignment
//
//  Created by ParkEasy on 9/3/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    stackValues = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Onebtn:(id)sender {
    [self addnumber:1];
}

- (IBAction)Twobtn:(id)sender {
    [self addnumber:2];
}

- (IBAction)Threebtn:(id)sender {
    [self addnumber:3];
}

- (IBAction)Fourbtn:(id)sender {
    [self addnumber:4];
}

- (IBAction)Fivebtn:(id)sender {
    [self addnumber:5];
}

- (IBAction)Sixbtn:(id)sender {
    [self addnumber:6];
}

- (IBAction)Sevenbtn:(id)sender {
    [self addnumber:7];
}

- (IBAction)Eightbtn:(id)sender {
    [self addnumber:8];
}

- (IBAction)Ninebtn:(id)sender {
    [self addnumber:9];
}

- (IBAction)Zerobtn:(id)sender {
    [self addnumber:0];
}

- (IBAction)Dotbtn:(id)sender {
    [self addpoint];
}

- (IBAction)ClearAllbtn:(id)sender {
    stackValues = @"";
    Result = 0.0;
    
    self.Displaylbl.text = stackValues;
    self.CalculatedDisplay.text = [NSString stringWithFormat:@"%g", Result];
}

- (IBAction)ClearBtn:(id)sender {
    [self addnumber:-1];
}

- (IBAction)Plusbtn:(id)sender {
    [self logic:1]; //addition
}

- (IBAction)Minusbtn:(id)sender {
    [self logic:2]; //minus
}

- (IBAction)Timesbtn:(id)sender {
    [self logic:3]; //times
}

- (IBAction)Dividebtn:(id)sender {
    [self logic:4]; //devide
}

- (IBAction)Calculatebtn:(id)sender {
    [self logic:0]; //equals
}

-(void)addnumber:(int)number{
    
    if(stackValues == NULL){
        stackValues = @"0";
    }
    
    if(number > -1){
        stackValues = [NSString stringWithFormat:@"%1$@%2$d", stackValues, number];
    }else if([stackValues length] > 0){
        stackValues = [stackValues substringToIndex:[stackValues length]-1];
        //Operator = [Operator substringToIndex:[Operator length]-1];
    }
    
    if([stackValues length] <= 0){
        stackValues = @"";
    }
    
    self.Displaylbl.text = stackValues;
}

-(void)addpoint{
    
    stackValues = [NSString stringWithFormat:@"%1$@.", stackValues];
    
    self.Displaylbl.text = stackValues;
    
}

-(void)logic:(int)operator{
    
//    func doMath(newOp: String) {
//        if userInput != "" && !numStack.isEmpty {
//            var stackOp = opStack.last
//            if !((stackOp == "+" || stackOp == "-") && (newOp == "*" || newOp == "/")) {
//                var oper = ops[opStack.removeLast()]
//                accumulator = oper!(numStack.removeLast(), accumulator)
//                doEquals()
//            }
//        }
//        opStack.append(newOp)
//        numStack.append(accumulator)
//        userInput = ""
//        updateDisplay()
//    }
    
    //OperatorStack.last
    
    //operatortype = operator;
    
    if(operator == 0){ // Equals
        if(operatortype == 1){
            stackValues = [NSString stringWithFormat:@"%.1f", Result + [stackValues doubleValue]];
        }else if(operatortype == 2){
            stackValues = [NSString stringWithFormat:@"%.1f", Result - [stackValues doubleValue]];
            
        }else if(operatortype == 3){
            stackValues = [NSString stringWithFormat:@"%.1f", Result * [stackValues doubleValue]];
            
        }else if(operatortype == 4){
            stackValues = [NSString stringWithFormat:@"%.1f", [stackValues doubleValue] / Result];
            
        }
        self.CalculatedDisplay.text = stackValues;
    }else{
        
        if(operator == 1){ // addition
            stackValues = [NSString stringWithFormat:@"%1$@+", stackValues];
            
        }else if(operator == 2){ // subtraction
            stackValues = [NSString stringWithFormat:@"%1$@-", stackValues];
        }else if(operator == 3){ // multiply
            stackValues = [NSString stringWithFormat:@"%1$@*", stackValues];
        }else if(operator == 4){ // division
            stackValues = [NSString stringWithFormat:@"%1$@/", stackValues];
        }
        operatortype = operator;
        Result = [stackValues doubleValue];
        self.CalculatedDisplay.text = stackValues;
        
    }
}
@end
