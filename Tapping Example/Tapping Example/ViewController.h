//
//  ViewController.h
//  Tapping Example
//
//  Created by ParkEasy on 9/2/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSTimer *timer;
    NSTimer *cdtimer;
    int CountInt;
    int tapInt;
    int CDInt;
}
@property (weak, nonatomic) IBOutlet UILabel *timerlbl;
@property (weak, nonatomic) IBOutlet UILabel *tapslbl;
- (IBAction)TAPbtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *TapOutlet;
- (IBAction)Startbtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Startoutlet;


@end

