//
//  ViewController.m
//  Tapping Example
//
//  Created by ParkEasy on 9/2/15.
//  Copyright (c) 2015 ParkEasy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    CountInt = 5;
    tapInt = 0;
    self.TapOutlet.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Startbtn:(id)sender {
    if (CountInt == 5) {
        self.timerlbl.text = [NSString stringWithFormat:@"ending in.. %i", CountInt];
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(StartCounter) userInfo:nil repeats:YES];
        tapInt = 0;
        self.TapOutlet.enabled = YES;
        self.Startoutlet.enabled = NO;
        [sender setTitle:@"GO!" forState:UIControlStateNormal];
    }
}

- (IBAction)TAPbtn:(id)sender {
    if (CountInt > 0) {
        tapInt += 1;
        self.tapslbl.text = [NSString stringWithFormat:@"TAPS: %i", tapInt];
    }
    if (CountInt < 1) {
        [timer invalidate];
        [sender setTitle:@"Wait" forState:UIControlStateNormal];
    }
}

- (void)StartCounter {
    CountInt -= 1;
    self.timerlbl.text = [NSString stringWithFormat:@"ending in.. %i", CountInt];
    
    if (CountInt < 1) {
        [timer invalidate];
        CountInt = 5;
        self.Startoutlet.enabled = YES;
        self.TapOutlet.enabled = NO;
        self.timerlbl.text = [NSString stringWithFormat:@"Ready again?"];
        [self.Startoutlet setTitle:@"Play" forState:UIControlStateNormal];
    }
}
@end
